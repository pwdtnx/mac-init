#!/bin/sh

set -eu

brew_prefix="/usr/local/bin"
brew="$brew_prefix/brew"
provision_dir=~/.provision

attention() {
  printf "\e[1;7;34m==> %s \e[0m\n" "$*"
}

information() {
  printf "\e[1;34m    %s \e[0m\n" "$*"
}

ecmd() {
  local prompt_mark="\e[1;34m>"
  if [ "$1" = 'sudo' ]; then
    prompt_mark="\e[1;7;31m#"
  fi

  printf "${prompt_mark} "

  for a in $@; do
    if echo $a | fgrep -sq ' '; then
      printf "%s " "'$a'"
    else
      printf "%s " "$a"
    fi
  done

  printf "\e[0m\n"
}

dmce() {
  printf "\n"
}

ecdo() {
  ecmd $@
    "$@"
  dmce
}

if ! ([ -s ~/.ssh/id_rsa ] && [ -s ~/.ssh/id_rsa.pub ]); then
  attention "Generating SSH key pair..."
  ecdo ssh-keygen -t rsa -b 2048
  cat ~/.ssh/id_rsa.pub | pbcopy
  information "Done. The genetrated public key in id_rsa.pub is copied in the clipboard."
  information "Register it for remote hosts and/or repositories such as GitHub and/or Bitbucket."
  information "Press [Enter] to proceed."
  read ___WAIT
else
  attention "SSH key pair exists."
fi

if [ ! -f "$brew" ]; then
  attention Install Homebrew
  ecmd '/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"'
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  dmce

  ecdo $brew doctor
  ecdo $brew update
else
  attention Homebrew is installed.
fi

attention Fetch provision reository
/usr/bin/git clone "$REPO" "$provision_dir"

/bin/sh "$provision_dir/init.sh"

attention Done.
